<?php
require('../vendor/autoload.php');

$app = new Silex\Application();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints as Assert;

//$request = Request::createFromGlobals();
$request = new Request(
    $_GET,
    $_POST,
    array(),
    $_COOKIE,
    $_FILES,
    $_SERVER
);


$app['debug'] = true;


use Silex\Provider\FormServiceProvider;
$app->register(new FormServiceProvider());

use Silex\Provider\ValidatorServiceProvider;
$app->register(new Silex\Provider\ValidatorServiceProvider());


$app->register(new Silex\Provider\SessionServiceProvider());





// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

use Silex\Provider\TwigServiceProvider;
// Register the Twig templating engine
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path' => __DIR__.'/../views',
));


$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));


$dbopts = parse_url(getenv('DATABASE_URL'));
$app->register(new Herrera\Pdo\PdoServiceProvider(),
  array(
    'pdo.dsn' => 'pgsql:dbname='.ltrim($dbopts["path"],'/').';host='.$dbopts["host"],
    'pdo.port' => $dbopts["port"],
    'pdo.username' => $dbopts["user"],
    'pdo.password' => $dbopts["pass"]
  )
);


//echo $app['twig']->render('header.twig');

if (null === $user = $app['session']->get('user')) {
    $userin = false;
}else{
	$name = $user["username"];
	$userin = true;
}
echo $app['twig']->render('header.twig', array('name' => $name,
	        'userin' => $userin));
// Our web handlers

$app->match('/logout', function(Request $request) use($app) {
		$app['session']->remove('user');
		$app['monolog']->addDebug('volt user sess');
		//return $app->redirect('/account');
		//return "Viszlát!";
		return $app->redirect('/');
});


$app->match('/unsubscribe', function(Request $request) use($app) {

		if (null === $user = $app['session']->get('user')) {
		    $userin = false;
		    return "Bocs, ez nem fog menni.";
		}else{
			$name = $user["username"];
			$userid = $user["userid"];
			//ide jön a felhasználó törlése a db-ből
			//ide jön az üzenetek törlése a db-ből
			/*
				$st = $app['pdo']->prepare("SELECT name, email FROM users WHERE name LIKE '".$uname."' AND  password LIKE '".$upw."' LIMIT 1");
  				$st->execute();
			*/
  			$st = $app['pdo']->prepare('DELETE FROM forum_table WHERE userid='.(int)$userid.';');
			$st->execute();
			$st = $app['pdo']->prepare('DELETE FROM users WHERE id='.(int)$userid.';');
			$st->execute();
			$app['session']->remove('user');
			$app['monolog']->addDebug('Leiratkozás');
			return "Sikeres Leiratkozás.";
		}

		
});


$app->match('/', function(Request $request) use($app) {
	
  //$app['monolog']->addDebug('logging output.');
	$forum = Forum($app);
  	$app['monolog']->addDebug('Itt vagyok látod, ez az a hely.');
  	if (null === $user = $app['session']->get('user')) {
        $app['monolog']->addDebug('Nincs user sess');
        $userin = false;
    }else{
    	$app['monolog']->addDebug('Van user sess');
    	$name = $user["username"];
    	$userid = $user["userid"];
    	$userin = true;
    }

    if($userin && $userid){
    	$forum_form = $app['form.factory']->createBuilder('form', $data)
        ->add('content', 'text', array(
	        'constraints' => array(new Assert\NotBlank())
	    ))
        ->getForm();

	    $forum_form->handleRequest($request);

	    if ($forum_form->isValid()) {
	        $data = $forum_form->getData();

	        $app['monolog']->addDebug('forum form isValid + ok');

	        //$name = $data["name"] . " " . $data["password"];
	        $dc = strip_tags($data["content"]);
	        $dc = str_replace('\'','"',$dc);
	        $dc = addslashes($dc);
	        AddFormContent($dc,$userid, $app);
	        
	        return $app->redirect('/');
	        
	    }
    }


    $form = $app['form.factory']->createBuilder('form', $data)
        ->add('name', 'text', array(
	        'constraints' => array(new Assert\Length(array('min' => 5)))
	    ))
        ->add('password', 'password', array(
	        'constraints' => array(new Assert\NotBlank())
	    ))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
        $data = $form->getData();

        $app['monolog']->addDebug('isValid + ok');

        //$name = $data["name"] . " " . $data["password"];
        $validUserData = ValidUser($data["name"],$data["password"],$app);
        $name = $validUserData["name"];
        return $app->redirect('/');
    }



	if($userin){
		 return $app['twig']->render('index.twig', array(
	        'name' => $name,
	        'userin' => $userin,
	        'form' => $form->createView(),
	        'forum_form' => $forum_form->createView()
	    )).$forum;

	}else{
		 return $app['twig']->render('index.twig', array(
	        'name' => $name,
	        'userin' => $userin,
	        'form' => $form->createView(),
	        'forum_form' => ''
	    )).$forum;

	}

 
 
});



$app->get('/twig/{name}', function ($name) use ($app) {
    return $app['twig']->render('tw.twig', array(
        'name' => $name,
        'userin' => false
    ));
});

$app->get('/account', function (Request $request) use ($app) {
    

	if (null === $user = $app['session']->get('user')) {
        return $app->redirect('/');
    }
   
   return "Sikeres belépés! Szia " . $user["username"]. "! :)";
   
});

$app->match('/reg', function (Request $request) use ($app) {
    

	if (null === $user = $app['session']->get('user')) {
		 $reg_form = $app['form.factory']->createBuilder('form', $data)
        ->add('name', 'text', array(
	        'constraints' => array(new Assert\Length(array('min' => 5)))
	    ))
        ->add('fullname', 'text', array(
	        'constraints' => array(new Assert\Length(array('min' => 5)))
	    ))
        ->add('email', 'text', array(
	        'constraints' => array(new Assert\Email(array(
            		'message' => 'A megadott  "{{ value }}" emailcím nem szabályos.',
            		'checkMX' => true,
        	))
	    )))
        ->add('password', 'repeated', array(
		    'type' => 'password',
		    'invalid_message' => 'A két jelszó nem egyezik meg.',
		    'options' => array('attr' => array('class' => 'password-field')),
		    'required' => true,
		    'first_options'  => array('label' => 'Password'),
		    'second_options' => array('label' => 'Repeat Password'),
		))
        ->getForm();

	    $reg_form->handleRequest($request);

	    if ($reg_form->isValid()) {
	        $data = $reg_form->getData();

	        $app['monolog']->addDebug('isValid + ok');
	        //var_dump($data);
	        if(AddUser($data,$app)){
	        	ValidUser($data["name"],$data["password"],$app);
	        	return $app->redirect('/');	
	        	//return "ok";
	        }else{
	        	return "Valami hiba történt a regisztráció során. Próbáld újra.";
	        }
	        
	        //$name = $validUserData["name"];
	        
	        //return $app->redirect('/');
	    }  

	    return $app['twig']->render('reg.twig', array(
	        'reg_form' => $reg_form->createView()
	    ));

    }else{
    	return $app->redirect('/');
    }
   
});



$app->get('/forum/', function() use($app) {
	$forum = Forum($app);
	return $forum;
});





$app->get('/forum_del/{ftid}/{uid}', function($ftid,$uid) use($app) {
  //$st = $app['pdo']->prepare('SELECT name FROM test_table');
	if (null === $user = $app['session']->get('user')) {
		    $userid = 0;
		}else{
			$name = $user["username"];
			$userid = $user["userid"];
		}
	if($userid!=0 && $userid == $uid){
		//return "Ok: ".$userid. " - ".$ftid;
		//ezt még meg kell írni rendesen!! törlés
		
		//$st = $app['pdo']->prepare('SELECT ft.id as ftid, userid, name, forum_text, ft.ts FROM forum_table as ft INNER JOIN users as u ON ft.userid=u.id  ORDER BY ts DESC LIMIT 20');
		$st = $app['pdo']->prepare('DELETE FROM forum_table WHERE userid='.(int)$userid.' AND id='.(int)$ftid.';');
		$st->execute();

		return $app->redirect('/');
	}
	return "Bocs, ez nem fog menni.";
	
});



$app->get('/db/', function() use($app) {
  //$st = $app['pdo']->prepare('SELECT name FROM test_table');
	$st = $app['pdo']->prepare('SELECT name, email FROM users');
  $st->execute();

  $names = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $app['monolog']->addDebug('Row ' . $row['name']);
    $names[] = $row;
  }

  return $app['twig']->render('database.twig', array(
    'names' => $names
  ));
});

//user + pw lekérdezés
function ValidUser($uname,$upw,$app) {
  //$st = $app['pdo']->prepare('SELECT name FROM test_table');
	$st = $app['pdo']->prepare("SELECT id, name, email FROM users WHERE name LIKE '".$uname."' AND  password LIKE '".$upw."' LIMIT 1");
  $st->execute();

  $names = '';
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $app['monolog']->addDebug('Row ' . $row['name']);
    $names = $row;
    $username = $names['name'];
    $userid = $names['id'];
  	$app['session']->set('user', array('username' => $username,'userid' => $userid), 1800);
  }
  
  
  //return $names;
};


function Forum($app) {
	if (null === $user = $app['session']->get('user')) {
		    $userid = 0;
		}else{
			$name = $user["username"];
			$userid = $user["userid"];
		}
	$st = $app['pdo']->prepare('SELECT ft.id as ftid, userid, name, forum_text, ft.ts FROM forum_table as ft INNER JOIN users as u ON ft.userid=u.id  ORDER BY ts DESC LIMIT 20');
  $st->execute();

  $forum_rows = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $app['monolog']->addDebug('Row ' . $row['name']);
    if($userid>0 && $userid ==$row['userid']){
    	$row["del"] = '/forum_del/'.$row['ftid'].'/'.$row['userid'].'';
    }else{
    	$row["del"] = false;
    }
    $row['forum_text'] = stripslashes($row['forum_text']);
    $forum_rows[] = $row;


  }

  return $app['twig']->render('forum.twig', array(
    'forum_rows' => $forum_rows
  ));

}
function AddFormContent($dc, $uid, $app){
	if (null === $user = $app['session']->get('user')) {
	    $userid = 0;
	}else{
		$name = $user["username"];
		$userid = $user["userid"];
	}
	if($uid!=0 && $uid==$userid){
		$st = $app['pdo']->prepare('INSERT INTO forum_table (userid, forum_text) values ( '.$uid.', \''.$dc.'\');');
  		$st->execute();
	}
}

function AddUser($data,$app){
	
	foreach ($data as $key => $value){
	   $data[$key]  = legyjo($value);
	}
	//echo 'INSERT INTO users (name, fullname, password, email) values (  \''.$data['name'].'\',\''.$data['fullname'].'\',\''.$data['password'].'\',\''.$data['email'].'\');';
	$st = $app['pdo']->prepare('INSERT INTO users (name, fullname, password, email) values (  \''.$data['name'].'\',\''.$data['fullname'].'\',\''.$data['password'].'\',\''.$data['email'].'\');');
  	if($st->execute()){
  		return true;
  	}else{
  		return false;
  	}

}

function legyjo($dc){
	$dc = strip_tags($dc);
    $dc = str_replace('\'','"',$dc);
    $dc = addslashes($dc);
    return $dc;
}

echo $app['twig']->render('footer.twig');
$app->run();

?>